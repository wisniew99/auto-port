#!/bin/bash

########################
#                      #
##  Auto-port script  ##
##      by wisniew99  ##
##              2016  ##
#                      #
########################
#
# Auto-port script for i9300.
# By wisniew99 / rafciowis1999
# You can use and edit this as You want. :)
# I NOT allowing to steal this scripts!
# Please save orginal author.
#
# If You will public Your project at xda
# or other public usage,
# Write somewhere authors of this script.
# Thanks!


################
# Request:
# zip
# Python 3.x
################

################
# Put ROM that You want to PORT in port folder.
# Rename it to port.zip
################
# Put ROM that You want as BASE in base folder.
# Rename it to base.zip
################

################
# Run script with ./run.sh
################
##### Wait #####
################
# And grab new zip from done folder
################

################
# This file is only for unzip and detect block or non-block build.
################

echo ""
echo ""
echo "Auto-port script by wisniew99"
echo ""
echo ""

if [ -e "port/port.zip" ]
then
    echo "port.zip detected."
    echo ""
    echo "unzipping port.zip..."
    echo ""
    unzip port/port.zip -d port/port/
    echo ""

    if [ -e "port/port/META-INF" ]
    then
        echo "Unzipped successfully."
        echo ""
        echo "Detecting ROM structure..."
        echo ""

        if [ -e "port/port/system.new.dat" ]
        then
            echo "Detected block structure of port."
            echo ""

            if [ -e "base/base.zip" ]
            then
                echo "base.zip detected."
                echo ""
                echo "Unzipping base.zip..."
                echo ""
                unzip base/base.zip -d base/base/
                echo ""

                if [ -e "base/base/META-INF" ]
                then
                    echo "Unzipped successfully."
                    echo ""
                    echo "Detecting ROM structure..."
                    echo ""

                    if [ -e "base/base/system.new.dat" ]
                    then
                        echo "Detected block structure of base."
                        echo ""
                        echo "Starting blockblock.sh..."
                        echo ""
                        ./scripts/blockblock.sh
                        echo ""
                        echo "Auto-port script by wisniew99"
                        echo ""
                        echo "Thanks for using auto-port."
                        echo ""
                        exit 1
                    else
                        echo "Detected non-block structure of base."
                        echo ""
                        echo "Starting blocknon.sh..."
                        echo ""
                        ./scripts/blocknon.sh
                        echo ""
                        echo "Auto-port script by wisniew99"
                        echo ""
                        echo "Thanks for using auto-port."
                        echo ""
                        exit 1
                    fi
                else
                    echo ""
                    echo "Unzipped NOT successfully"
                    echo "Zip file is corrupted"
                    echo "or This isn't full ROM"
                    echo ""
                    exit 1
                fi
            else
                echo ""
                echo "base.zip NOT detected."
                echo "make sure You correctly rename it"
                echo "or base.zip is in base folder"
                echo ""
                exit 1
            fi
        else
            echo "Detected non-block structure of port."
            echo ""

            if [ -e "base/base.zip" ]
            then
                echo "base.zip detected."
                echo ""
                echo "Unzipping base.zip..."
                echo ""
                unzip base/base.zip -d base/base/
                echo ""

                if [ -e "base/base/META-INF" ]
                then
                    echo "Unzipped successfully."
                    echo ""
                    echo "Detecting ROM structure..."
                    echo ""

                    if [ -e "base/base/system.new.dat" ]
                    then
                        echo "Detected block structure of base."
                        echo ""
                        echo "Starting nonblock.sh..."
                        echo ""
                        ./scripts/nonblock.sh
                        echo ""
                        echo "Auto-port script by wisniew99"
                        echo ""
                        echo "Thanks for using auto-port."
                        echo ""
                        exit 1
                    else
                        echo "Detected non-block structure of base."
                        echo ""
                        echo "Starting nonnon.sh..."
                        echo ""
                        ./scripts/nonnon.sh
                        echo ""
                        echo "Auto-port script by wisniew99"
                        echo ""
                        echo "Thanks for using my script."
                        echo ""
                        exit 1
                    fi
                else
                    echo ""
                    echo "Unzipped NOT successfully"
                    echo "Zip file is corrupted"
                    echo "or This isn't full ROM"
                    echo ""
                    exit 1
                fi
            else
                echo ""
                echo "base.zip NOT detected."
                echo "make sure You correctly rename it"
                echo "or base.zip is in base folder"
                echo ""
                exit 1
            fi
        fi
    else
        echo ""
        echo "Unzipped NOT successfully"
        echo "Zip file is corrupted"
        echo "or This isn't full ROM"
        echo ""
        exit 1
    fi
else
    echo ""
    echo "port.zip NOT detected."
    echo "make sure You correctly rename it"
    echo "or port.zip is in port folder"
    echo ""
    exit 1
fi
