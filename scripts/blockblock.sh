#!/bin/bash

########################
#                      #
##  Auto-port script  ##
##      by wisniew99  ##
##              2016  ##
#                      #
########################
#
# Auto-port script for i9300.
# By wisniew99 / rafciowis1999
# You can use and edit this as You want. :)
# I NOT allowing to steal this scripts!
# Please save orginal author.
#
# This is file for block block porting.


BASEOUT="base/base/output"
PORTOUT="port/port/output"

echo ""
echo ""
echo "Started block block script."
echo ""
echo ""
echo ""
echo "Working on port files:"
echo "dat > img"

cp tools/sdat2img.py port/port/
cd port/port/
./sdat2img.py system.transfer.list system.new.dat system.img
rm sdat2img.py

echo "Mounting img..."

mkdir output
sudo mount -t ext4 -o loop system.img output/

if [ -e output ]
then
    echo ""
    echo "Mounted correctly."
    cd -
    echo "Working on base files:"
    echo "dat > img"

    cp tools/sdat2img.py base/base/
    cd base/base/
    ./sdat2img.py system.transfer.list system.new.dat system.img
    rm sdat2img.py

    echo "Mounting img..."

    mkdir output
    sudo mount -t ext4 -o loop system.img output/

    if [ -e output ]
    then
        echo ""
        echo "Mounted correctly."
        cd -
        echo ""
        echo "All mounted correctly."
        echo ""
        echo "Starting coping files."/system
        echo ""
        echo "Please wait..."
        echo "This can take a few minutes."
        echo ""


        sudo rm -r $BASEOUT/app
        sudo rm -r $BASEOUT/fonts
        sudo rm -r $BASEOUT/framework
        sudo rm -r $BASEOUT/media
        sudo rm -r $BASEOUT/priv-app

        sudo cp $PORTOUT/priv-app $BASEOUT/
        sudo cp $PORTOUT/app $BASEOUT/
        sudo cp $PORTOUT/fonts $BASEOUT/
        sudo cp $PORTOUT/framework $BASEOUT/
        sudo cp $PORTOUT/media $BASEOUT/


        sudo rm -r $BASEOUT/etc/permissions/platform.xml
        sudo rm -r $BASEOUT/etc/permissions/handheld_core_hardware.xml
        sudo rm -r $BASEOUT/etc/permissions/handheld_hardware.xml

        sudo cp $PORTOUT/etc/permissions/platform.xml $BASEOUT/etc/permissions/
        sudo cp $PORTOUT/etc/permissions/handheld_core_hardware.xml $BASEOUT/etc/permissions/
        sudo cp $PORTOUT/etc/permissions/handheld_hardware.xml $BASEOUT/etc/permissions/


        sudo rm -r $BASEOUT/etc/init.d/00banner
        sudo cp $PORTOUTm/etc/init.d/00banner $BASEOUT/etc/init.d/


        touch tmp.txt
        CATTMP='cat tmp.txt'

        function escape_slashes {
            sed 's/\//\\\//g'
        }

        function change_line {
            local OLD_LINE_PATTERN=$1; shift
            local NEW_LINE=$1; shift
            local FILE=$1

            local NEW=$(echo "${NEW_LINE}" | escape_slashes)
            sudo sed -i .bak '/'"${OLD_LINE_PATTERN}"'/s/.*/'"${NEW}"'/' "${FILE}"
            sudo mv "${FILE}.bak" /tmp/
        }

        sudo grep -e 'ro.build.id=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.build.id=" "$CATTMP" $BASEOUT/build.prop

        sudo grep -e 'ro.build.display.id=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.build.display.id=" "$CATTMP" $BASEOUT/build.prop

        sudo grep -e 'ro.build.version.release=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.build.version.release=" "$CATTMP" $BASEOUT/build.prop

        sudo grep -e 'ro.product.model=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.product.model=" "$CATTMP" $BASEOUT/build.prop

        sudo grep -e 'ro.config.ringtone=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.config.ringtone=" "$CATTMP" $BASEOUT/build.prop

        sudo grep -e 'ro.config.notification_sound=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.config.notification_sound=" "$CATTMP" $BASEOUT/build.prop

        sudo grep -e 'ro.config.alarm_alert=' $PORTOUT/build.prop > tmp.txt
        sudo change_line "ro.config.alarm_alert=" "$CATTMP" $BASEOUT/build.prop


        rm base/base/system.patch.dat
        rm base/base/system.img

        rm port/port/system.patch.dat
        rm port/port/system.img

        cp tools/make_ext4fs base/base/
        cd base/base/
        ./make_ext4fs -T 0 -S file_contexts -l 1073741824 -a system system_new.img output/
        rm make_ext4fs
        cd -

        cp tools/rimg2sdat base/base/
        cd base/base/
        ./rimg2sdat system.patch.dat
        rm rimg2sdat

        sudo rm -r output
        zip -r done *

        cd -

        sudo rm -r port/port/output

        mkdir done
        mv base/base/done.zip done/

        rm -rf base/base
        rm -rf port/port

        if [ -e done/done.zip ]
        then
            echo "ALL DONE!"
            echo "Grab file from done"
            echo "Buckup before flashing!"
        else
            echo "Something goes wrong."
            echo "Try redownload script and try again."
            exit 1
        fi

    else
        echo ""
        echo "Mounted incorrectly"
        exit 1
    fi
else
    echo ""
    echo "Mounted incorrectly"
    exit 1
fi




